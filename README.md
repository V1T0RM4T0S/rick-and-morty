# Rick and Morty

<a href="https://standardjs.com"><img src="https://img.shields.io/badge/code_style-standard-brightgreen.svg" alt="Standard - JavaScript Style Guide"></a>

## What is this?

This is an Angular project that consumes the [Rick and Morty REST API](https://rickandmortyapi.com/)

You can access it [here](https://rick-and-morty-production.up.railway.app/characters/page/1)

## Screenshots

On Desktop:

![desktop-dark](./screenshots/desktop-dark.png)

On Mobile:

<img src="./screenshots/mobile-dark.png" style="width:400px; height:auto"/>

Theming:

![theming](./screenshots/theming.png)

## Copyright

Rick and Morty is created by Justin Roiland and Dan Harmon for Adult Swim. The data and images are used without claim of ownership and belong to their respective owners.

This application is open source and uses a Creative Commons CC0 1.0 Universal license.
